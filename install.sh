#!/bin/sh

# Script to install Melissa software

git clone -b SC23 https://gitlab.inria.fr/melissa/melissa.git
cd melissa
sudo apt install -y cmake build-essential libopenmpi-dev python3.8 libzmq3-dev
mkdir build install && cd build
cmake -DNATIVE_PIP_INSTALL=OFF -DCMAKE_INSTALL_PREFIX=../install ..
make
make install
cd ..
python3 -m venv .venv
source .venv/bin/activate
python3 -m pip install --upgrade pip
pip3 install -r requirements.txt -r requirements_deep_learning.txt -r requirements_dev.txt
pip3 install -e .
source melissa_set_env.sh
echo "melissa-launcher -h"
echo "melissa-server -h"

